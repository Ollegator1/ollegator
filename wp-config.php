<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'prod_endowed_wisdom');

/** MySQL database username */
define('DB_USER', 'endowed_wisdom');

/** MySQL database password */
define('DB_PASSWORD', 'Gq898m');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '_3B3vCICNP01(=*,zEQQ]aw{G*v&.CXH9D 3NXb6RZ4]SFA-9QhN97&V*/,5${~I');
define('SECURE_AUTH_KEY',  'M9?LFQ||sFS6c5xopSn1r_|fwjbwr0GO_c=;zoz1QUyDzzzEY9Be-8MvN@&fL<7[');
define('LOGGED_IN_KEY',    '}nzos]#d_,$d{S!GIMqq|vx.:_<8xg5woEYIiyz(>l;h$J*V5dMJprNuXm<s6vP,');
define('NONCE_KEY',        'pvRWE`hHR4~o!q&CcACems0iS=C&AWX^ik)Z{F?78;I5]E]4f]{q-l^|!R_^`Og!');
define('AUTH_SALT',        '!l3^:@C(L,ou&zghZ;L%hkPkX=^SB2;FC@$z(JHNJ 4%{Q2#cW11.Yk:kF.oRF5&');
define('SECURE_AUTH_SALT', 'B^NL8`UkPyDGKPA1aiaot]tS+g|#Hz!z#bZ<PDE{]#8l}DP%esTZk#HH/K5/[fV<');
define('LOGGED_IN_SALT',   ')j2<5TXO;H}W,U:tMS6v@.KY=2h58-}h`];9$OSba62M-eTrh7s:sU2{Um+g<e|g');
define('NONCE_SALT',       '^t }jyVr+CW8|>6&g>[RX;Qm|)Nps~Q{/JD1<uS^yV<-s)]b*Km*<yM4iI&2 viO');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
